package com.azoft.sberbank.security;

import com.azoft.sberbank.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
public final class LoginHelper {

    private LoginHelper() {
    }

    /**
     * Получение авторизованого юзера из SecurityContextHolder
     * @return пользователь
     */
    public static User getUser() {
        return getFromAuth(SecurityContextHolder.getContext().getAuthentication());
    }

    /**
     * Получение авторизованого юзера из SecurityContextHolder
     * @return пользователь
     */
    private static User getFromAuth(final Authentication auth) {
        User user = null;
        if (null != auth) {
            if (null != auth.getPrincipal()
                    && auth.getPrincipal() instanceof User) {
                user = (User) auth.getPrincipal();
            } else {
                log.warn("Cannot extract ClientUserInfo from principal " + auth.getPrincipal());
            }
        } else {
            log.warn("auth is null");
        }
        return user;
    }
}
