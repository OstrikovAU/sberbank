package com.azoft.sberbank.dto;

import com.azoft.sberbank.integration.schema.EntityInfo;

public class UserInfo {

    /**
     * id_ow из пц
     */
    private String id;
    /**
     * Статус пользователя в пц
     */
    private String status;

    public UserInfo(EntityInfo entityInfo) {
        id = entityInfo.getIdentities().get(0).getId();
        status = entityInfo.getState();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
