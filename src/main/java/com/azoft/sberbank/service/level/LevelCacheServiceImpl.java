package com.azoft.sberbank.service.level;

import com.azoft.sberbank.dto.LevelCache;
import com.azoft.sberbank.dto.SettingDto;
import com.azoft.sberbank.entity.Level;
import com.azoft.sberbank.service.setting.SettingWebService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.Instant;
import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
public class LevelCacheServiceImpl implements LevelCacheService {

    private static final String LEVEL_CACHE = "level";
    private final CacheManager cacheManager;
    private final SettingWebService settingWebService;

    /**
     * Метод получения уровня привелегии из кэша
     * @param idData (id_ow)
     * @return Уровень привелегии из кэша
     */
    @Override
    public LevelCache getLevel(String idData) {
        Cache cache = cacheManager.getCache(LEVEL_CACHE);
        LevelCache levelCache = cache.get(idData, LevelCache.class);
        SettingDto setting = settingWebService.getActualSetting();
        Instant timeExpired = ZonedDateTime.now().minusMinutes(setting.getTimeCache()).toInstant();
        if (ObjectUtils.isEmpty(levelCache) || levelCache.isExpired(timeExpired)) {
            cache.evict(idData);
            return null;
        }
        return levelCache;
    }

    /**
     * Кэширование уровня привелегии
     * @param idData (id_ow)
     * @param level уровень привелегии
     */
    @Override
    public void caching(String idData, Level level) {
        Cache cache = cacheManager.getCache(LEVEL_CACHE);
        cache.put(idData, new LevelCache(level, Instant.now()));
    }
}
