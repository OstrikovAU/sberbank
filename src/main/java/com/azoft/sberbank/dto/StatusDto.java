package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class StatusDto {

    /**
     * Заголовок статуса
     */
    private String title;
    /**
     * Значение статуса
     */
    private String value;

    public StatusDto(Status status) {
        this.title = status.getDescription();
        this.value = status.name();
    }
}
