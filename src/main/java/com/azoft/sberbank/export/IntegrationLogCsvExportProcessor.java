package com.azoft.sberbank.export;

import com.azoft.sberbank.entity.IntegrationLog;
import com.azoft.sberbank.service.log.IntegrationLogService;

public class IntegrationLogCsvExportProcessor extends AbstractCsvExportProcessor<IntegrationLog, IntegrationLogService> {

    public IntegrationLogCsvExportProcessor(IntegrationLogService service, String encoding) {
        super(service, encoding);
    }

    /**
     * Метод формирования строк данных
     * @param bean сущность
     */
    @Override
    void exportEntity(IntegrationLog bean) {
        getCsvExport()
                .row()
                .addCell(bean.getCreated())
                .addCell(bean.getType().getDescription())
                .addCell(bean.getStatus().getDescription())
                .addCell(bean.getMethodName())
                .addCell(bean.getParams())
                .build();
    }

    /**
     * Метод формирования заголовка файла
     */
    @Override
    void createTableHead() {
        getCsvExport()
                .row()
                .addCell("Дата и время")
                .addCell("Тип операции")
                .addCell("Статус")
                .addCell("Метод")
                .addCell("Параметры")
                .build();
    }
}
