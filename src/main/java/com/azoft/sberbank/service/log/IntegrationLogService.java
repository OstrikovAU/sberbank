package com.azoft.sberbank.service.log;

import com.azoft.sberbank.entity.IntegrationLog;
import com.azoft.sberbank.service.Service;

import java.time.Instant;
import java.util.List;

public interface IntegrationLogService extends LogService<IntegrationLog, Long> {

    List<IntegrationLog> find(Instant start, Instant end);
}
