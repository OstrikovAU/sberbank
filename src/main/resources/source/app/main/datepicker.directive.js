function DatepickerDirective() {

    return{
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elm, attr, ctrl){

            ctrl.$formatters.unshift(function(value) {
                if(value && moment(value).isValid()){
                    return moment(new Date(value)).format('dd.MM.yyyy');
                }
                return value;
            });

            scope.$watch(attr.ngDisabled, function (newVal) {
                if(newVal === true)
                    $(elm).datepicker("disable");
                else
                    $(elm).datepicker("enable");
            });

            elm.datepicker({
                autoSize: true,
                changeYear: true,
                changeMonth: true,
                dateFormat: 'dd.mm.yy',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
                yearRange: "1900:2019",
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                showOn: 'focus',
                buttonText: '<i class="glyphicon glyphicon-calendar"></i>',
                onSelect: function (valu) {
                    scope.$apply(function () {
                        ctrl.$setViewValue(valu);
                    });
                    elm.focus();
                }
            });
        }
    }
}

module.exports = DatepickerDirective;