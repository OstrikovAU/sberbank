function ClientDirective() {
    return  {
        restrict: 'E',
        templateUrl: 'client/client.html',
        scope: {
            isServiceMode: '='
        }
    };
}

module.exports = ClientDirective;