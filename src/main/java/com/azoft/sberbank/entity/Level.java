package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Level {

    MORE_THAN_THANKS("Больше, чем Спасибо"),
    THANKS("Спасибо"),
    MUCH_THANKS("Большое спасибо"),
    SO_MUCH_THANKS("Огромное спасибо");

    /**
     * Заголовок уровня привелегии
     */
    private final String title;

    public static Level parse(String value) {
        switch (value) {
            case "1":
                return THANKS;
            case "2":
                return MUCH_THANKS;
            case "3":
                return SO_MUCH_THANKS;
            case "4":
                return MORE_THAN_THANKS;
            default:
                return null;
        }
    }
}
