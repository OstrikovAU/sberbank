package com.azoft.sberbank.repository;

import com.azoft.sberbank.entity.IntegrationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface IntegrationLogRepository extends JpaRepository<IntegrationLog, Long> {

    List<IntegrationLog> findByCreatedBetweenOrderByCreated(Instant start, Instant end);
}
