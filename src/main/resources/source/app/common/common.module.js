const notifyDirective = require('./notify/notify.directive');
const notifyController = require('./notify/notify.controller');
const loaderDirective = require('./loader/loader.directive');
const loaderController = require('./loader/loader.controller');
const phoneFormat = require('./filters/phone-format.filter');

const common = angular.module('common', []);

common.directive('notify', notifyDirective);
common.directive('loader', loaderDirective);
common.controller('notifyController', notifyController);
common.controller('loaderController', loaderController);
common.filter('phoneFormat', phoneFormat);