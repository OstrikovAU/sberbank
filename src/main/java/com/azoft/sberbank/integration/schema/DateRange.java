package com.azoft.sberbank.integration.schema;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Data
public class DateRange implements Serializable {

    public static final ZoneId CLIENT_ZONE_ID = ZoneId.of("Europe/Moscow");

    @JsonIgnore
    private static final long serialVersionUID = 1851252783683112640L;

    /**
     * Левая граница временного диапазона
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Europe/Moscow")
    private ZonedDateTime startDate;

    /**
     * Правая граница временного диапазона
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Europe/Moscow")
    private ZonedDateTime endDate;


    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate.withZoneSameInstant(CLIENT_ZONE_ID);
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate.withZoneSameInstant(CLIENT_ZONE_ID);
    }
}
