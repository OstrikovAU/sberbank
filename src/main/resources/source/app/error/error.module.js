const error = angular.module('error', []);

error.config(config);

function config($routeProvider) {

    $routeProvider.when('/error', {
        templateUrl: 'error/error.html'
    });
}