package com.azoft.sberbank.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {

    /**
     * Телефон
     */
    String phone;
    /**
     * Пароль
     */
    String password;
    /**
     * Дата рождения
     */
    String birthDate;
}
