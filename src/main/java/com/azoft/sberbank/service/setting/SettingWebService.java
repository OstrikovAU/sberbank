package com.azoft.sberbank.service.setting;

import com.azoft.sberbank.dto.SettingDto;

public interface SettingWebService {

    SettingDto getActualSetting();

    SettingDto update(SettingDto dto);
}
