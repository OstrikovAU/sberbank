function LoaderController($rootScope) {

    const self = this;

    $rootScope.$on('loader:start', loaderStart);
    $rootScope.$on('loader:stop', loaderStop);

    function loaderStart() {
        self.showLoader = true;
    }

    function loaderStop() {
        self.showLoader = false;
    }
}

module.exports = LoaderController;