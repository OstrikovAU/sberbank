package com.azoft.sberbank.service.export;

public interface ExportService {

    byte[] exportUserLogs();
    byte[] exportIntegrationLog();
}
