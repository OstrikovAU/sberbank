package com.azoft.sberbank.repository;

import com.azoft.sberbank.entity.Role;
import com.azoft.sberbank.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByPhone(String phone);

    List<User> findAllByRole(Role role);

    User findByPhoneAndBirthDate(String phone, String birthDate);
}
