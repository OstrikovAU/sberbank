package com.azoft.sberbank.service.log;

import com.azoft.sberbank.dto.LogDto;
import com.azoft.sberbank.dto.Topic;
import com.azoft.sberbank.entity.IntegrationLog;
import com.azoft.sberbank.entity.UserLog;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoggingServiceImpl implements LoggingService {

    private final UserLogService userLogService;
    private final IntegrationLogService integrationLogService;
    private final KafkaTemplate<Long, LogDto> kafkaTemplate;

    /**
     * Метод отправки данных в kafka
     * @param log лог
     * @param topic название топика
     */
    @Async
    public void send(LogDto log, Topic topic) {
        kafkaTemplate.send(topic.name(), log);
    }

    /**
     * Метод получения лога действий пользователя и сохранение в бд
     * @param log
     */
    @KafkaListener(topics = "USER_LOG", groupId = "group_id")
    @Override
    public void userLogLogging(LogDto log) {
        userLogService.saveOrUpdate(new UserLog(log));
    }

    /**
     * Метод получения лога запросов в пц и сохранение в бд
     * @param log
     */
    @KafkaListener(topics = "INTEGRATION_LOG", groupId = "group_id")
    @Override
    public void integrationLogLogging(LogDto log) {
        integrationLogService.saveOrUpdate(new IntegrationLog(log));
    }

}
