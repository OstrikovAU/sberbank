package com.azoft.sberbank.controller.auth;

import com.azoft.sberbank.dto.LoginRequest;
import com.azoft.sberbank.dto.RedirectDto;
import com.azoft.sberbank.service.auth.AuthWebService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Slf4j
public class LoginRestController {

    private final AuthWebService authWebService;

    /***
     * Метода api для авторизации пользователей
     * @param loginRequest объект с данными для авторизации (номер телефона, дата рождения, пароль)
     * @return страница для перехода (админку, страницу пользователя, страницу с ошибкой или страницу заглушку сервисного режима)
     */
    @PostMapping("/login")
    public RedirectDto login(@RequestBody LoginRequest loginRequest) {
        log.info("Автаризация пользователя {}", loginRequest.getPhone());
        String page = authWebService.login(loginRequest);
        log.info("Перенаправление на страницу {}", page);
        return new RedirectDto(page);
    }

    /***
     * Метод api для logout
     */
    @GetMapping("/logout")
    public void logout() {
        authWebService.logout();
    }
}
