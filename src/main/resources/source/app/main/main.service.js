function MainService(api) {

    const self = this;
    self.urls = {
        setting: '/admin/setting',
        login: '/auth/login',
        logout: '/auth/logout',
        userLog: '/admin/userLog',
        integrationLog: '/admin/integrationLog',
        user: '/user',
        users: '/users'

    };

    self.loadUser = loadUser;
    self.login = login;
    self.logout = logout;
    self.getSetting = getSetting;
    self.saveSetting = saveSetting;
    self.getUsers = getUsers;

    function getSetting() {
        return api.request('get', self.urls.setting);
    }

    function saveSetting(params) {
        return api.request('post', self.urls.setting, params);
    }

    function login(params) {
        return api.request('post', self.urls.login, params);
    }

    function logout() {
        return api.request('get', self.urls.logout)
    }

    function loadUser() {
        return api.request('get', self.urls.user);
    }

    function getUsers() {
        return api.request('get', self.urls.users);
    }
}

module.exports = MainService;