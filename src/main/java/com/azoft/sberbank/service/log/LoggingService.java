package com.azoft.sberbank.service.log;

import com.azoft.sberbank.dto.LogDto;
import com.azoft.sberbank.dto.Topic;

public interface LoggingService {

    void send(LogDto log, Topic topic);

    void userLogLogging(LogDto log);

    void integrationLogLogging(LogDto log);
}
