package com.azoft.sberbank.service.cft;

import com.azoft.sberbank.dto.UserInfo;

public interface CftService {

    UserInfo getUserInfo(String phone, String birthDate);
}
