package com.azoft.sberbank.controller.user;

import com.azoft.sberbank.dto.UserDto;
import com.azoft.sberbank.service.user.UserWebService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserRestController {

    private final UserWebService userWebService;

    /***
     * Метод api для получения данных авторизованного пользователя
     * @return UserDto данные пользователя
     */
    @GetMapping("/user")
    public UserDto getUser() {
        return userWebService.getUser();
    }

    /***
     * Метод api для получения всех пользователей
     * @return список данных пользователей
     */
    @GetMapping("/users")
    public List<UserDto> getUsers() {
        return userWebService.getUsers();
    }
}
