const loginController = require('./login.controller');

const login = angular.module('login', []);

login.controller('loginController', loginController);

login.config(config);

function config($routeProvider) {

    $routeProvider.when('/login', {
        templateUrl: 'login/login.html',
        controller: 'loginController',
        controllerAs: 'loginCtrl'
    });
}