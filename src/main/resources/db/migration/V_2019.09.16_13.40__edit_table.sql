CREATE SEQUENCE user_log_seq START WITH 1;
CREATE SEQUENCE integration_log_seq START WITH 1;
CREATE SEQUENCE user_seq START WITH 1;
CREATE SEQUENCE setting_seq START WITH 1;

create table app_setting (
                         id bigint primary key,
                         on_service_mode boolean not null,
                         require_date boolean not null,
                         time_cache int not null
);

insert into app_setting (id, on_service_mode, require_date, time_cache) VALUES (1, false, true, 0);

alter table users rename column level to user_level;