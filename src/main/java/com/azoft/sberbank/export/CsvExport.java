package com.azoft.sberbank.export;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CsvExport {
    private static final Logger logger = LoggerFactory.getLogger(CsvExport.class);
    private ByteArrayOutputStream baos;
    private CSVPrinter csvPrinter;
    private String NEW_LINE_SEPARATOR = "\n";
    private char DELIMITER = ';';

    public CsvExport(String encoding) {
        baos = new ByteArrayOutputStream();
        Writer out = new OutputStreamWriter(baos, Charset.forName(encoding));
        CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR).withDelimiter(DELIMITER);
        try {
            csvPrinter = new CSVPrinter(out, csvFormat);
        } catch (IOException e) {
            logger.error("Error create CSVPrinter: " , e);
        }
    }

    /**
     * Метод формирования новой строки
     * @return Builder для формирования строки
     */
    public CsvRowBuilder row() {
        return new CsvRowBuilder(csvPrinter);
    }

    /**
     * Метод возврата записаных данных
     * @return записаные данные
     * @throws IOException
     */
    public ByteArrayOutputStream write() throws IOException {
        csvPrinter.flush();
        csvPrinter.close();
        return baos;
    }

    /**
     * Builder для формирования строк файла
     */
    public static class CsvRowBuilder {

        /**
         * Объект для записи колонок в строку
         */
        private CSVPrinter csvPrinter;
        /**
         * Список колонок
         */
        private List<Object> cells = new ArrayList();

        public CsvRowBuilder(CSVPrinter printer) {
            csvPrinter = printer;
        }

        /**
         * Метод записи колонки
         * @param value объект для записи
         * @return Builder для формирования строк файла
         */
        public CsvRowBuilder addCell(Object value) {
            cells.add(value != null ? value: "");
            return this;
        }

        /**
         * Метод записи колонки с датой
         * @param date объект для записи
         * @return Builder для формирования строк файла
         */
        public CsvRowBuilder addCell(Instant date) {
            cells.add(date != null ? DateTimeFormatter.ISO_INSTANT.format(date) : "");
            return this;
        }

        /**
         * Метод запуска построения строки
         */
        public void build() {
            try {
                csvPrinter.printRecord(cells);
            } catch (IOException e) {
                logger.error("Error write row in file: " , e);
            }
        }
    }
}
