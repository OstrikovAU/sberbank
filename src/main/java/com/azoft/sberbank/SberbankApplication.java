package com.azoft.sberbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SberbankApplication {

    public static void main(String[] args) {
        SpringApplication.run(SberbankApplication.class, args);
    }

}
