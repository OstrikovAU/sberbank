package com.azoft.sberbank.integration;

import com.azoft.sberbank.dto.UserInfo;
import com.azoft.sberbank.entity.Level;

import java.math.BigDecimal;

public interface CftIntegrationService {

    UserInfo userInfoQuery(String phone, String birthDate);

    BigDecimal balanceQuery(String idData);

    Level levelQuery(String idData);
}
