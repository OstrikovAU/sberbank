package com.azoft.sberbank.service.log;

import com.azoft.sberbank.entity.AEntity;
import com.azoft.sberbank.service.Service;

import java.time.Instant;
import java.util.List;

public interface LogService<E extends AEntity, ID extends Long> extends Service<E, ID> {

    List<E> find(Instant start, Instant end);
}
