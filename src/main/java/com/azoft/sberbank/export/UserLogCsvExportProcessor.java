package com.azoft.sberbank.export;

import com.azoft.sberbank.entity.UserLog;
import com.azoft.sberbank.service.log.UserLogService;

public class UserLogCsvExportProcessor extends AbstractCsvExportProcessor<UserLog, UserLogService> {

    public UserLogCsvExportProcessor(UserLogService service, String encoding) {
        super(service, encoding);
    }

    /**
     * Метод формирования строк данных
     * @param bean сущность
     */
    @Override
    void exportEntity(UserLog bean) {
        getCsvExport()
                .row()
                .addCell(bean.getCreated())
                .addCell(bean.getType().getDescription())
                .addCell(bean.getParams())
                .build();
    }

    /**
     * Метод формирования заголовка файла
     */
    @Override
    void createTableHead() {
        getCsvExport()
                .row()
                .addCell("Дата и время")
                .addCell("Тип операции")
                .addCell("Параметры")
                .build();
    }
}
