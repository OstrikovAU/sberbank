package com.azoft.sberbank.service.setting;

import com.azoft.sberbank.entity.Setting;
import com.azoft.sberbank.service.Service;

public interface SettingService extends Service<Setting, Long> {
}
