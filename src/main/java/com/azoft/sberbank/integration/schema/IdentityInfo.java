package com.azoft.sberbank.integration.schema;

import lombok.Data;

import java.io.Serializable;

@Data
public class IdentityInfo implements Serializable {
    private static final long serialVersionUID = -1825836211236325085L;
    /**
     * Идентификатор
     */
    private String id;
}