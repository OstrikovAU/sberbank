package com.azoft.sberbank.service.log;

import com.azoft.sberbank.entity.IntegrationLog;
import com.azoft.sberbank.repository.IntegrationLogRepository;
import com.azoft.sberbank.service.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class IntegrationLogServiceImpl extends ServiceImpl<IntegrationLog, Long, IntegrationLogRepository> implements IntegrationLogService {

    private final IntegrationLogRepository repository;

    @Override
    protected IntegrationLogRepository getRepository() {
        return repository;
    }

    /**
     * Метод получения логов за определеный период
     * @param start дата начала
     * @param end дата окончания
     * @return список логов
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<IntegrationLog> find(Instant start, Instant end) {
        return getRepository().findByCreatedBetweenOrderByCreated(start, end);
    }
}
