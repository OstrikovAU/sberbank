const gulp    = require('gulp');
const gulpif = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const addStream = require('add-stream');
const concat = require('gulp-concat');
const del = require('del');
const inject = require('gulp-inject');
const uglify = require('gulp-uglify');
const templateCache = require('gulp-angular-templatecache');
const stylus = require('gulp-stylus');
const nib = require('nib');
const htmlmin = require('gulp-htmlmin');
const cssnano = require('gulp-cssnano');
const argv = require('yargs').argv;

const ngAnnotate = require('gulp-ng-annotate');
const connect = require('gulp-connect');
const plumber = require('gulp-plumber');

const CacheBuster = require('gulp-cachebust');
const cachebust = new CacheBuster();

const htmlMinConfig = {
  removeComments: true,
  collapseWhitespace: true,
  removeAttributeQuotes: true,
  removeRedundantAttributes: true,
  removeOptionalTags: true,
  minifyJS: true
};

gulp.task('clean:css', cleanCss);
gulp.task('clean:js', cleanJs);
gulp.task('clean', clean);

gulp.task('compile:js', compileJs);
gulp.task('compile:stylus', compileStylus);
gulp.task('copy:images', copyImages);
gulp.task('copy:fonts', copyFonts);
gulp.task('copy:libraries', copyLibraries);
gulp.task('dev:environment', setDevEnvironment);

gulp.task('server', startServer);

gulp.task('build', ['clean', 'copy:fonts', 'copy:images', 'copy:libraries', 'compile:js', 'compile:stylus'], copyIndex);

gulp.task('watcher:css', ['clean:css', 'compile:stylus'], copyIndex);
gulp.task('watcher:js', ['clean:js', 'compile:js'], copyIndex);
gulp.task('default', ['dev:environment', 'build']);

gulp.task('test', function() {
  return prepareTemplates().pipe(gulp.dest('./app'));
});

function startServer() {
  connect.server({
    port: 9999,
    root: 'app',
    livereload: !!argv.development
  });
}


function isDevelopment(stream) {
    return gulpif(!!argv.development, stream);
}

function isProduction(stream) {
    return gulpif(!argv.development, stream);
}


function clean() {
  del.sync(['./src/main/resources/static/app']);
  console.log('[--------] App folder was deleted');
}


function copyIndex() {
  var libsInjects = gulp.src([
    './src/main/resources/static/app/libs/jquery.min.js',
    './src/main/resources/static/app/libs/jquery-ui.min.js',
    './src/main/resources/static/app/libs/angular.min.js',
    './src/main/resources/static/app/libs/angular-route.js'
  ], {read: false});

  return gulp.src('./src/main/resources/source/index.html')
    .pipe(plumber())
    .pipe(inject(libsInjects, {starttag: '<!-- inject:dependencies:js -->', ignorePath: 'src/main/resources/static/app', addRootSlash: false}))
    .pipe(htmlmin(htmlMinConfig))
    .pipe(cachebust.references())
    .pipe(gulp.dest('./src/main/resources/static/app'))
    .pipe(connect.reload());
}

function compileJs() {
  return browserify({entries: './src/main/resources/source/app/app.js', debug: !!argv.development}).bundle()
    .pipe(plumber())
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(isDevelopment(sourcemaps.init({loadMaps: true})))
    .pipe(ngAnnotate({single_quotes: true}))
    .pipe(addStream.obj(prepareTemplates()))
    .pipe(concat('app.js'))
    .pipe(cachebust.resources())
    .pipe(isProduction(uglify()))
    .pipe(isDevelopment(sourcemaps.write('./')))
    .pipe(gulp.dest('./src/main/resources/static/app'));
}

function compileStylus() {
  return gulp.src('./src/main/resources/source/styles/styles.styl')
    .pipe(plumber())
    .pipe(isDevelopment(sourcemaps.init()))
    .pipe(stylus({
      'paths':  ['node_modules', 'source/styles'],
      'import': ['nib', 'stylus-type-utils'],
      'use': [nib()],
      'include css': true
    }))
    .pipe(cachebust.resources())
    .pipe(isProduction(cssnano({safe: true})))
    .pipe(isDevelopment(sourcemaps.write('./')))
    .pipe(gulp.dest('./src/main/resources/static/app/styles'));
}


function copyImages() {
  return gulp.src(['./src/main/resources/source/images/**/*.*'])
    .pipe(plumber())
    .pipe(gulp.dest('./src/main/resources/static/app/images'));
}

function copyFonts() {
  return gulp.src(['./src/main/resources/source/fonts/**/*.*'])
    .pipe(plumber())
    .pipe(gulp.dest('./src/main/resources/static/app/fonts'));
}

function copyLibraries() {
  return gulp.src([
      './node_modules/jquery/dist/jquery.min.js',
      './node_modules/angular/angular.min.js',
      './node_modules/angular-route/angular-route.js',
      './src/main/resources/source/libs/**/*.js'
    ])
    .pipe(plumber())
    .pipe(gulp.dest('./src/main/resources/static/app/libs/'));
}

function cleanCss() {
  del.sync(['./src/main/resources/static/app/styles/**']);
}

function cleanJs() {
  del.sync(['./src/main/resources/static/app/app.*']);
}

function setDevEnvironment() {
  argv.development = true;
}

function prepareTemplates() {
  return gulp.src('./src/main/resources/source/app/**/*.html')
    .pipe(htmlmin(htmlMinConfig))
    .pipe(templateCache({standalone: true}));
}
