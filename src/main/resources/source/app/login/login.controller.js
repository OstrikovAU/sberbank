function LoginController($scope, $rootScope, moment, mainService, $location) {

    const self = this;
    self.login = login;
    self.invalid = false;
    self.isInValidDate = false;
    self.request = {};
    self.loginForm = {};

    $scope.$watch('loginCtrl.request.password', changePhoneOrPassword);
    $scope.$watch('loginCtrl.request.phone', changePhoneOrPassword);
    $scope.$watch('loginCtrl.request.birthDate', validateDate);

    function changePhoneOrPassword() {
        delete self.errorMsg;
        self.invalidLoginOrPassword = false;
    }

    function validateDate() {
        if (self.request.birthDate && self.request.birthDate.length === 10) {
            self.isInValidDate = !moment(self.request.birthDate, 'dd.MM.yyyy').isValid();
        } else {
            self.isInValidDate = false;
        }
    }

    function login() {
        self.invalid = self.loginForm.$invalid;
        if (!self.invalid && !self.isInValidDate) {
            $rootScope.$emit('loader:start');
            mainService.login(self.request)
                .then(success)
                .catch(error);
        }
    }

    function success(response) {
        $rootScope.$emit('loader:stop');
        $location.path(response.page);
    }

    function error(response) {
        if (response && response.errorMsg) {
            self.errorMsg = response.errorMsg;
            self.invalidLoginOrPassword = true;
            $rootScope.$emit('loader:stop');
        }
    }
}

module.exports = LoginController;