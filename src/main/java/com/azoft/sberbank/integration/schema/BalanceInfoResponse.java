package com.azoft.sberbank.integration.schema;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class BalanceInfoResponse {

    /**
     * Бонусный баланс
     */
    private BonusBalance bonusBalance;

    @Getter
    @Setter
    public static class BonusBalance {

        /**
         * Бонусов всего (active + inactive)
         */
        private BigDecimal total;
        /**
         * Активных бонусов (доступных к списанию, без учета ограничений)
         */
        private BigDecimal active;
        /**
         * Отложенных бонусов
         */
        private BigDecimal inactive;
        /**
         * Бонусная задолженность (если больше 0, то все остальные элементы равны 0)
         */
        private BigDecimal debt;
    }
}
