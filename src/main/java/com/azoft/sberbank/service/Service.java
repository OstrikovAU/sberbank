package com.azoft.sberbank.service;

import com.azoft.sberbank.entity.AEntity;

import java.util.List;

public interface Service<E extends AEntity, ID extends Long> {

    E saveOrUpdate(E entity);

    E findById(ID id);
}
