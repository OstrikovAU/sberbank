package com.azoft.sberbank.service.level;

import com.azoft.sberbank.dto.LevelCache;
import com.azoft.sberbank.entity.Level;
import com.azoft.sberbank.integration.CftIntegrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class LevelServiceImpl implements LevelService {

    private final CftIntegrationService cftIntegrationService;
    private final LevelCacheService levelCacheService;

    /**
     * Метод получения уровня привелегии пользователя
     * @param idData (id_ow)
     * @return уровень привелегии
     */
    @Override
    public Level getLevel(String idData) {
        LevelCache levelCache = levelCacheService.getLevel(idData);
        Level level;
        if (ObjectUtils.isEmpty(levelCache)) {
            level = cftIntegrationService.levelQuery(idData);
            levelCacheService.caching(idData, level);
        } else {
            level = levelCache.getLevel();
        }
        return level;
    }
}
