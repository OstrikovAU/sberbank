package com.azoft.sberbank.entity;

import com.azoft.sberbank.dto.LogDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "user_logs")
@NoArgsConstructor
public class UserLog extends AEntity {

    /**
     * id записи в бд
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_log_generator")
    @SequenceGenerator(name="user_log_generator", sequenceName = "user_log_seq")
    private Long id;
    /**
     * Дата создания
     */
    private Instant created = Instant.now();
    /**
     * Тип действия
     *     LOGIN("Авторизация")
     *     LOGOUT("Деавторизация")
     *     EXPORT("Выгрузка файла")
     *     CHANGE_SETTING("Изменение настроек")
     */
    @Enumerated(EnumType.STRING)
    private LogType type;
    /**
     * Параметры
     */
    private String params;

    public UserLog(LogDto dto) {
        this.type = dto.getType();
        this.params = dto.getParams();
    }
}
