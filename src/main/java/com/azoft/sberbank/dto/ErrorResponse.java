package com.azoft.sberbank.dto;

import lombok.Getter;

@Getter
public class ErrorResponse {

    /**
     * Сообщение об ошибке
     */
    private String errorMsg;

    public ErrorResponse(String message) {
        this.errorMsg = message;
    }
}
