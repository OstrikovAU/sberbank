function HeaderDirective() {
    return  {
        restrict: 'E',
        templateUrl: 'header/header.html',
        scope: {},
        controller: 'headerController',
        controllerAs: 'headerCtrl',
        bindToController: true
    };
}

module.exports = HeaderDirective;