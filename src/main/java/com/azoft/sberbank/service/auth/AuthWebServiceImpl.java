package com.azoft.sberbank.service.auth;

import com.azoft.sberbank.dto.*;
import com.azoft.sberbank.entity.*;
import com.azoft.sberbank.security.LoginHelper;
import com.azoft.sberbank.service.balance.BalanceService;
import com.azoft.sberbank.service.cft.CftService;
import com.azoft.sberbank.service.level.LevelService;
import com.azoft.sberbank.service.log.LoggingService;
import com.azoft.sberbank.service.setting.SettingWebService;
import com.azoft.sberbank.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import static java.util.Collections.singletonList;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthWebServiceImpl implements AuthWebService {

    private static final String EMPTY = "";
    private final AuthenticationProvider authenticationProvider;
    private final CftService cftService;
    private final UserService userService;
    private final SettingWebService settingWebService;
    private final BalanceService balanceService;
    private final LevelService levelService;
    private final LoggingService loggingService;

    /**
     * Метод авторизации пользователя
     * @param loginRequest данные введеные в форме арторизации
     * @return страница для редиректа
     */
    public String login(LoginRequest loginRequest) {
        User user = userService.findUser(loginRequest.getPhone());
        SettingDto setting = settingWebService.getActualSetting();
        if (Role.ROLE_ADMIN.equals(user.getRole())) {
            user = userService.findUser(loginRequest.getPhone(), loginRequest.getPassword(), EMPTY);
        } else if (setting.isOnServiceMode()) {
            log.info("Включен сервисный режим. serviceMode={}", setting.isOnServiceMode());
            return "tech";
        } else {
            if (setting.isRequireDate()) {
                log.info("Авторизация пользователя {} по паре логин/дата рождения", loginRequest.getPhone());
                user = userService.findUser(loginRequest.getPhone(), EMPTY, loginRequest.getBirthDate());
            } else {
                log.info("Авторизация пользователя {} по паре логин/пароль", loginRequest.getPhone());
                user = userService.findUser(loginRequest.getPhone(), loginRequest.getPassword(), EMPTY);
            }
            String birthDate = setting.isRequireDate() ? loginRequest.getBirthDate(): null;
            UserInfo userInfo = cftService.getUserInfo(loginRequest.getPhone(), birthDate);
            Level level = levelService.getLevel(userInfo.getId());
            Balance balance = balanceService.getBalance(userInfo.getId());
            user.setStatus(Enum.valueOf(Status.class, userInfo.getStatus()));
            user.setIdData(userInfo.getId());
            user.setLevel(level);
            user.setBalance(balance);
            user = userService.saveOrUpdate(user);
        }
        if (Status.D.equals(user.getStatus())) {
            log.error("Ошибка авторизации пользователя {}. Статус={}", loginRequest.getPhone(), user.getStatus().name());
            return "error";
        }
        Authentication authenticate = authenticationProvider.authenticate(
                new UsernamePasswordAuthenticationToken(user,
                        user.getPassword(),
                        singletonList(new SimpleGrantedAuthority(user.getRole().name()))));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        log.info("Пользователь {} успешно авторизовался", user.getPhone());
        loggingService.send(new LogDto(LogType.LOGIN, "user: " + user.getPhone()), Topic.USER_LOG);
        return user.getRole().getPage();
    }

    /**
     * Метод деавторизации пользователя
     */
    public void logout() {
        User user = LoginHelper.getUser();
        if (user != null) {
            SecurityContextHolder.clearContext();
            loggingService.send(new LogDto(LogType.LOGOUT, "user: " + user.getPhone()), Topic.USER_LOG);
        }
    }
}
