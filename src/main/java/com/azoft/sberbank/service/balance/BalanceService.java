package com.azoft.sberbank.service.balance;

import com.azoft.sberbank.entity.Balance;

public interface BalanceService {

    Balance getBalance(String idData);
}
