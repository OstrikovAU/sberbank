package com.azoft.sberbank.service.log;

import com.azoft.sberbank.entity.UserLog;
import com.azoft.sberbank.repository.UserLogRepository;
import com.azoft.sberbank.service.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor

public class UserLogServiceImpl extends ServiceImpl<UserLog, Long, UserLogRepository> implements UserLogService {

    private final UserLogRepository repository;

    @Override
    protected UserLogRepository getRepository() {
        return repository;
    }

    /**
     * Метод получения логов за определеный период
     * @param start дата начала
     * @param end дата окончания
     * @return список логов
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<UserLog> find(Instant start, Instant end) {
        return getRepository().findByCreatedBetweenOrderByCreated(start, end);
    }
}
