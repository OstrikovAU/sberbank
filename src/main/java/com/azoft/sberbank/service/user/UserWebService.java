package com.azoft.sberbank.service.user;


import com.azoft.sberbank.dto.UserDto;

import java.util.List;

public interface UserWebService {

    UserDto getUser();

    List<UserDto> getUsers();
}
