package com.azoft.sberbank.service.user;

import com.azoft.sberbank.entity.Role;
import com.azoft.sberbank.entity.User;
import com.azoft.sberbank.service.Service;

import java.util.List;

public interface UserService extends Service<User, Long> {

    User findUser(String phone, String password, String birthDate);

    User findUser(String phone);

    List<User> findByRole(Role role);
}
