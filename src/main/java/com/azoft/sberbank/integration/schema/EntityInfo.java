package com.azoft.sberbank.integration.schema;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class EntityInfo implements Serializable {
    private static final long serialVersionUID = 6802851113172212127L;
    /**
     * Тип сущности. Возможные значения:
     * CARD – карта
     * CLIENT – идентификационные данные клиента во внешней системе.
     */
    private String type;
    /**
     * Системный статус сущности. Возможные значения:
     * A – активный
     * D – временно заблокирован
     * R – ограничено списание баллов
     * L – заблокирован (окончательный статус)
     */
    private String state;
    /**
     * Идентификаторы идентификационной сущности
     */
    private List<IdentityInfo> identities;
}
