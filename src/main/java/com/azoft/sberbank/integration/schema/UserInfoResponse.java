package com.azoft.sberbank.integration.schema;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
public class UserInfoResponse implements Serializable {
    private static final long serialVersionUID = -4399681867358766387L;

    /**
     * Идентификационные сущности клиента
     */
    private List<EntityInfo> entities;
}
