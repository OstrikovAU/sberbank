package com.azoft.sberbank.entity;

import com.azoft.sberbank.dto.LogDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "integration_logs")
@NoArgsConstructor
public class IntegrationLog  extends AEntity {

    /**
     * id записи в бд
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "integration_log_generator")
    @SequenceGenerator(name="integration_log_generator", sequenceName = "integration_log_seq")
    private Long id;

    /**
     * Дата создания записи
     */
    private Instant created = Instant.now();

    /**
     * Тип действия
     *     REQUEST("Запрос")
     *     RESPONSE("Ответ")
     */
    @Enumerated(EnumType.STRING)
    private LogType type;
    /**
     * Статус
     *     OK("Успех")
     *     NO_OK("Неуспех")
     *     ERROR("Ошибка")
     */
    @Enumerated(EnumType.STRING)
    private LogStatus status;
    /**
     * Название вызваного метода
     */
    @Column(name = "method_name")
    private String methodName;
    /**
     * Параметры запроса или результат ответа метода
     */
    private String params;

    public IntegrationLog(LogDto dto) {
        this.type = dto.getType();
        this.status = dto.getStatus();
        this.methodName = dto.getMethodName();
        this.params = dto.getParams();
    }
}
