package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.Balance;
import com.azoft.sberbank.entity.User;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

@Getter
@Setter
public class UserDto {

    /**
     * Уровень привелегии
     */
    private LevelDto level;
    /**
     * Роль пользователя
     */
    private String role;
    /**
     * Телефон
     */
    private String phone;
    /**
     * Дата рождения
     */
    private String birthDate;
    /**
     * Количество бонусов
     */
    private BigDecimal balance;
    /**
     * Дата последнего обновления баланса
     */
    private Instant balanceUpdated;
    /**
     * Статус
     */
    private StatusDto status;

    public UserDto(User user) {
        this.level = new LevelDto(user.getLevel());
        this.phone = user.getPhone();
        this.birthDate = user.getBirthDate();
        Optional.ofNullable(user.getBalance()).map(Balance::getAmount).ifPresent(this::setBalance);
    }

    public UserDto(User user, Balance balance) {
        this.level = new LevelDto(user.getLevel());
        this.phone = user.getPhone();
        this.role = user.getRole().name();
        this.status = Optional.ofNullable(user.getStatus()).map(StatusDto::new).orElse(new StatusDto());
        this.level = Optional.ofNullable(user.getLevel()).map(LevelDto::new).orElse(new LevelDto());
        Optional.ofNullable(balance).map(Balance::getAmount).ifPresent(this::setBalance);
        Optional.ofNullable(balance).map(Balance::getUpdated).ifPresent(this::setBalanceUpdated);
    }
}
