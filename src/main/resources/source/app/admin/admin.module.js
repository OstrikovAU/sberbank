const adminController = require('./admin.controller');

const admin = angular.module('admin', []);
admin.controller('adminController', adminController);
admin.config(config);

function config($routeProvider) {

    $routeProvider.when('/admin', {
        templateUrl: 'admin/admin.html',
        controller: 'adminController',
        controllerAs: 'adminCtrl',
        resolve: {
            user: getUser,
            setting: getSetting
        }
    });
}

function getSetting(mainService) {
    return mainService.getSetting();
}

function getUser(mainService) {
    return mainService.loadUser()
        .then(function (response) {
            return response;
        })
        .catch(function (response) {
            return null;
        });
}