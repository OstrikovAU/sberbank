function AdminController($rootScope, $scope, mainService, user, setting, config) {

    const self = this;
    self.state = 'setting';
    self.user = user;
    self.setting = setting;
    self.apiUrl = config.apiUrl;
    self.changeState = changeState;
    self.saveSetting = saveSetting;
    self.downloadUserLogs = downloadUserLogs;
    self.downloadIntegrationLogs = downloadIntegrationLogs;


    $scope.$watch('adminCtrl.setting.timeCache', validateTimeCache);
    $rootScope.$emit('update', self.setting, self.user);

    init();

    function validateTimeCache() {
        self.isInvalidTimeCache = self.setting.timeCache < 0 || self.setting.timeCache > 60;
    }

    function init() {
        if (self.user) {
            mainService.getUsers().then(function (response) {
                self.users = response;
            })
        }
    }

    function downloadUserLogs() {
        window.open(self.apiUrl + mainService.urls.userLog, '_blank');
    }

    function downloadIntegrationLogs() {
        window.open(self.apiUrl + mainService.urls.integrationLog, '_blank');
    }

    function saveSetting() {
        if (!self.isInvalidTimeCache) {
            $rootScope.$emit('loader:start');
            mainService.saveSetting(self.setting)
                .then(function () {
                    $rootScope.$emit('loader:stop');
                    $rootScope.$emit('notifySuccess:show');
                }).catch(function () {
                $rootScope.$emit('loader:stop');
                $rootScope.$emit('notifyError:show');
            });
        }
    }

    function changeState(state) {
        self.state = state;
    }
}

module.exports = AdminController;