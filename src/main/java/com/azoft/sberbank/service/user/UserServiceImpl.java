package com.azoft.sberbank.service.user;

import com.azoft.sberbank.entity.Role;
import com.azoft.sberbank.entity.User;
import com.azoft.sberbank.exception.UserNotFoundException;
import com.azoft.sberbank.repository.UserRepository;
import com.azoft.sberbank.service.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl extends ServiceImpl<User, Long, UserRepository> implements UserService {

    private final UserRepository repository;

    /**
     * Метод поиска пользователя
     * @param phone телефон
     * @param password пароль
     * @param birthDate дата рождения
     * @return пользователь
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public User findUser(String phone, String password, String birthDate) {
        log.info("Поиск пользователя {} в бд", phone);
        User user = null;
        if (StringUtils.isEmpty(password)) {
            user = repository.findByPhoneAndBirthDate(phone, birthDate);
        } else {
            User findUser = repository.findByPhone(phone);
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!ObjectUtils.isEmpty(findUser) && encoder.matches(password, findUser.getPassword())) {
                log.info("Введенный пользователем {} пароль совпадает", phone);
                user = findUser;
            }
        }
        if (ObjectUtils.isEmpty(user)) {
            log.error("Пользователь {} не найден в бд или же пароль или дата рождения({}) введены не верно", phone, birthDate);
            throw new UserNotFoundException("Пользователь не найден");
        }
        log.info("Пользователь {} найден в бд", phone);
        return user;
    }

    /**
     * Метод поиска юзера в бд по номеру телефона
     * @param phone телефон
     * @return пользователь
     */
    @Override
    public User findUser(String phone) {
        log.info("Поиск пользователя {} в бд", phone);
        User user = repository.findByPhone(phone);
        if (ObjectUtils.isEmpty(user)) {
            log.error("Пользователь {} не найден в бд", phone);
            throw new UserNotFoundException("Пользователь не найден");
        }
        log.info("Пользователь {} найден в бд", phone);
        return user;
    }

    /**
     * Метод поиска всех пользователей по роли
     * @param role роль
     * @return список пользователей
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<User> findByRole(Role role) {
        return repository.findAllByRole(role);
    }

    @Override
    protected UserRepository getRepository() {
        return repository;
    }
}
