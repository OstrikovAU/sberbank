create table users (
    id bigserial primary key,
    phone varchar(15) not null,
    password varchar(255) not null,
    birth_date varchar(10) not null,
    role varchar(20) not null,
    id_data varchar(10),
    status varchar(1),
    level varchar(30),
    balance NUMERIC(10,2),
    updated TIMESTAMP WITH TIME ZONE
);


insert into users (id, phone, password, birth_date, role)
values (1, '9999999999', '$2a$10$LRGJpkOiSZ.eIuZXXP5Ot.PJKla9Bf4tyJYBRlqvTsz.O2TmXa/GK', '01.01.2000', 'ROLE_ADMIN');

insert into users (id, phone, password, birth_date, role)
values (2, '9993333333', '$2a$10$SqvIT1t26iUgoJh3D/3Jdu/1f/z4bCoJfy4HjCdzFuZSmtlvcfMru', '15.04.1987', 'ROLE_CLIENT');

insert into users (id, phone, password, birth_date, role)
values (3, '9994444444', '$2a$10$VYMrBQpP2sXviJYjYAAmGe5EHFjOBexxYfpyjtV1rcjvE9xoUex6e', '11.12.1990', 'ROLE_CLIENT');

insert into users (id, phone, password, birth_date, role)
values (4, '9995555555', '$2a$10$0kp0jvZoNkVyT80TCcVwK.xa3mnZeUrHknpJV6U/eEfkddE2EP9/6', '07.07.1991', 'ROLE_CLIENT');

insert into users (id, phone, password, birth_date, role)
values (5, '9055164806', '$2a$10$716/3d3LPSy/yeiTevY5YeJJpvJfKfJdlBZjsu3rBzo8pb9apfp2q', '05.01.2000', 'ROLE_CLIENT');