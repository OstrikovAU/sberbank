package com.azoft.sberbank.dto;

/**
 * Название топиков для kafka
 */
public enum Topic {

    USER_LOG,
    INTEGRATION_LOG
}
