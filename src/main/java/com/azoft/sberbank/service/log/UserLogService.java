package com.azoft.sberbank.service.log;

import com.azoft.sberbank.entity.UserLog;
import com.azoft.sberbank.service.Service;

import java.time.Instant;
import java.util.List;

public interface UserLogService extends LogService<UserLog, Long> {

    List<UserLog> find(Instant start, Instant end);
}
