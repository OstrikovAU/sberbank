package com.azoft.sberbank.controller.admin;

import com.azoft.sberbank.dto.SettingDto;
import com.azoft.sberbank.service.export.ExportService;
import com.azoft.sberbank.service.setting.SettingWebService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;

@RestController
@RequestMapping("/api/admin")
@RequiredArgsConstructor
public class AdminRestController {

    private final SettingWebService settingWebService;
    private final ExportService exportService;

    /***
     * метод api получения актуальных настроек
     * @return SettingDto текущие настройки
     */
    @GetMapping("/setting")
    public SettingDto setting() {
        return settingWebService.getActualSetting();
    }

    /***
     * Метод api для обновления настроек
     * @param dto данные для обновления настроек
     * @return SettingDto обновленные настройки
     */
    @PostMapping("/setting")
    public SettingDto setting(@RequestBody SettingDto dto) {
        return settingWebService.update(dto);
    }

    /***
     * Метод api получения Лог-файл операций Участников
     * @return Лог-файл операций Участников
     */
    @GetMapping("/userLog")
    public ResponseEntity<InputStreamResource> getUserLog() {
        byte[] bytes = exportService.exportUserLogs();
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(bytes));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=logs.csv")
                .contentType(MediaType.valueOf("application/csv"))
                .contentLength(bytes.length)
                .body(resource);
    }

    /***
     * Метод api получения Лог-файл обмена запросами с ПЦ
     * @return Лог-файл обмена запросами с ПЦ
     */
    @GetMapping("/integrationLog")
    public ResponseEntity<InputStreamResource> getIntegrationLog() {
        byte[] bytes = exportService.exportIntegrationLog();
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(bytes));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=logs.csv")
                .contentType(MediaType.valueOf("application/csv"))
                .contentLength(bytes.length)
                .body(resource);
    }
}
