const tech = angular.module('tech', []);

tech.config(config);

function config($routeProvider) {

    $routeProvider.when('/tech', {
        templateUrl: 'tech/tech.html'
    });
}