function NotifyDirective() {
    return {
        restrict: 'E',
        scope: {},
        controller: 'notifyController',
        controllerAs: 'notifyCtrl',
        templateUrl: 'common/notify/notify.html'
    }
}

module.exports = NotifyDirective;