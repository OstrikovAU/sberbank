create table user_logs (
    id bigserial primary key,
    created TIMESTAMP WITH TIME ZONE,
    type varchar(20),
    params VARCHAR(5000)
);

create table integration_logs (
    id bigserial primary key,
    created TIMESTAMP WITH TIME ZONE,
    type varchar(20),
    status varchar(20),
    method_name varchar(255),
    params text
);