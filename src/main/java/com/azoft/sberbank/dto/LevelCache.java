package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.Level;
import lombok.Getter;

import java.time.Instant;

/**
 * Объект кэша уровня привелегии
 */
@Getter
public class LevelCache {

    /**
     * Уровень привелегии
     */
    private Level level;
    /**
     * Дата создания
     */
    private Instant created;

    public LevelCache(Level level, Instant created) {
        this.level = level;
        this.created = created;
    }

    public boolean isExpired(Instant timeExpired) {
        return created.isBefore(timeExpired);
    }
}
