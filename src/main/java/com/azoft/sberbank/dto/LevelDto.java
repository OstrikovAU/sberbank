package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.Level;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class LevelDto {

    /**
     * Заголовок уровня
     */
    private String title;
    /**
     * Значение уровня
     */
    private String value;

    public LevelDto(Level level) {
        Optional.ofNullable(level).map(Level::getTitle).ifPresent(this::setTitle);
        Optional.ofNullable(level).map(Level::name).ifPresent(this::setValue);
    }
}
