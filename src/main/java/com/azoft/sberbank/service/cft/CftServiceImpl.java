package com.azoft.sberbank.service.cft;

import com.azoft.sberbank.dto.UserInfo;
import com.azoft.sberbank.integration.CftIntegrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CftServiceImpl implements CftService {

    private final CftIntegrationService cftIntegrationService;

    /**
     * Метод получения информации о пользователе из пц
     * @param phone телефон
     * @param birthDate дата рождения
     * @return Информация о пользователе
     */
    @Override
    public UserInfo getUserInfo(String phone, String birthDate) {
        return cftIntegrationService.userInfoQuery(phone, birthDate);
    }
}
