require('angular-route');
require('./main/main.module');
require('./login/login.module');
require('angular-ui-mask');
require('./admin/admin.module');
require('./tech/tech.module');
require('./error/error.module');
require('./common/common.module');
require('moment/moment');
var moment = require('moment');

const app = angular.module('sberbank', [
  'ngRoute', 'ui.mask', 'common', 'main', 'login', 'admin', 'tech', 'error', 'templates'

]);

const localConfig = require('./local.config');
const stageConfig = require('./stage.config');
const wasStageConfig = require('./was-stage.config');

window.moment = moment;
app.constant('moment', moment);
app.constant('config', wasStageConfig);
app.config(config);

function config($locationProvider) {
  $locationProvider.hashPrefix('!');
}
