package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class Balance {

    /**
     * Количество бонусов
     */
    @Column(name = "balance")
    private BigDecimal amount;
    /**
     * Дата последнего запроса в пц для получения баланса
     */
    private Instant updated;

    public Balance(BigDecimal amount, Instant updated) {
        this.amount = amount;
        this.updated = updated;
    }
}
