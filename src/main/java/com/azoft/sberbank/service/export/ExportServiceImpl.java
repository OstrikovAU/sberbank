package com.azoft.sberbank.service.export;

import com.azoft.sberbank.dto.LogDto;
import com.azoft.sberbank.dto.Topic;
import com.azoft.sberbank.entity.LogType;
import com.azoft.sberbank.entity.User;
import com.azoft.sberbank.export.IntegrationLogCsvExportProcessor;
import com.azoft.sberbank.export.UserLogCsvExportProcessor;
import com.azoft.sberbank.security.LoginHelper;
import com.azoft.sberbank.service.log.IntegrationLogService;
import com.azoft.sberbank.service.log.LoggingService;
import com.azoft.sberbank.service.log.UserLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExportServiceImpl implements ExportService {

    private final IntegrationLogService integrationLogService;
    private final UserLogService userLogService;
    private final LoggingService loggingService;

    /**
     * Эксторт Лог-файл операций Участников
     * @return массив данных
     */
    public byte[] exportUserLogs() {
        byte[] bytes = new UserLogCsvExportProcessor(userLogService, "UTF-8").process();
        loggingService.send(new LogDto(LogType.EXPORT, "Лог-файл операций Участников"), Topic.USER_LOG);
        return bytes;
    }

    /**
     * Экспорт Лог-файл обмена запросами с ПЦ
     * @return массив данных
     */
    public byte[] exportIntegrationLog() {
        byte[] bytes = new IntegrationLogCsvExportProcessor(integrationLogService, "UTF-8").process();
        loggingService.send(new LogDto(LogType.EXPORT, "Лог-файл обмена запросами с ПЦ"), Topic.USER_LOG);
        return bytes;
    }
}
