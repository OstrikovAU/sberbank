package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Role {

    ROLE_ADMIN("admin"),
    ROLE_CLIENT("");

    /**
     * Страница для ридеректа
     */
    private final String page;
}
