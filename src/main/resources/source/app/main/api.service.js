function ApiService($http, $rootScope, config) {

  const self = this;
  self.baseUrl = config.apiUrl;
  self.request = request;

  function request(type, url, params) {
    const requestData = {};

    if (params) {
      if (type === 'get') {
        requestData.params = params;
      }
      else {
        angular.extend(requestData, params);
      }
    }
    return $http[type](self.baseUrl + url, requestData).then(getResponseData).catch(error);
  }


  function getResponseData(response) {
    if (response.status === 200 && (response.data || (response.data === ''))) {
      response.data = (response.data === '') ? {} : response.data;
      return response.data;
    }
    return response;
  }

  function error(response) {
    if (response.status === 502) {
      $rootScope.$emit('notifyError:show', 'Внутреняя ошибка сервер');
    }
    if (response.data || (response.data === '')) {
      response.data = (response.data === '') ? {} : response.data;
      throw response.data;
    }
    throw response;
  }
}
module.exports = ApiService;
