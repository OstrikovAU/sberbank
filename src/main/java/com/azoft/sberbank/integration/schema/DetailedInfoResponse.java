package com.azoft.sberbank.integration.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DetailedInfoResponse {

    /**
     * Список time-dependent атрибутов
     */
    private List<DetailedInfoAttributes> timeDependentAttributes;

    public List<DetailedInfoAttributes> getTimeDependentAttributes() {
        if (timeDependentAttributes == null) {
            timeDependentAttributes = new ArrayList<>();
        }
        return timeDependentAttributes;
    }
}
