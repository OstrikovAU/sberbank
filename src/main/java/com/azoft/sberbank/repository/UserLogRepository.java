package com.azoft.sberbank.repository;

import com.azoft.sberbank.entity.UserLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface UserLogRepository extends JpaRepository<UserLog, Long> {

    List<UserLog> findByCreatedBetweenOrderByCreated(Instant start, Instant end);
}
