package com.azoft.sberbank.integration.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetailedInfoAttrValue implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 638382985548946022L;

    /**
     * Значение time-dependent атрибута.
     */
    private String value;
}
