package com.azoft.sberbank.service.balance;

import com.azoft.sberbank.dto.BalanceCache;
import com.azoft.sberbank.dto.SettingDto;
import com.azoft.sberbank.service.setting.SettingWebService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
public class BalanceCacheServiceImpl implements BalanceCacheService {

    private static final String BALANCE_CACHE = "balance";
    private final CacheManager cacheManager;
    private final SettingWebService settingWebService;

    /**
     * Метод получения баланса из кэша
     * @param idData (id_ow)
     * @return Баланс из кэша
     */
    @Override
    public BalanceCache getBalance(String idData) {
        Cache cache = cacheManager.getCache(BALANCE_CACHE);
        BalanceCache balanceCache = cache.get(idData, BalanceCache.class);
        SettingDto setting = settingWebService.getActualSetting();
        Instant timeExpired = ZonedDateTime.now().minusMinutes(setting.getTimeCache()).toInstant();
        if (ObjectUtils.isEmpty(balanceCache) || balanceCache.isExpired(timeExpired)) {
            cache.evict(idData);
            return null;
        }
        return balanceCache;
    }

    /**
     * Метод кэширования баланса
     * @param idData (id_ow)
     * @param balance Баланс
     */
    @Override
    public void caching(String idData, BigDecimal balance) {
        Cache cache = cacheManager.getCache(BALANCE_CACHE);
        cache.put(idData, new BalanceCache(Instant.now(), balance));
    }
}
