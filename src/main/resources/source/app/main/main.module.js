const mainController = require('./main.controller');
const mainService = require('./main.service');
const clientDirective = require('../client/cilent.directive');
const headerController = require('../header/header.controller');
const headerDirective = require('../header/header.directive');
const apiService = require('./api.service');
const datepicker = require('./datepicker.directive');

const main = angular.module('main', []);

main.controller('mainController', mainController);

main.controller('headerController', headerController);
main.service('mainService', mainService);
main.service('api', apiService);
main.directive('client', clientDirective);
main.directive('header', headerDirective);
main.directive('datepicker', datepicker);

main.config(config);

function config($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'main/main.html',
        controller: 'mainController',
        controllerAs: 'mainCtrl',
        resolve: {
            user: getUser
        }
    });
}

function getUser(mainService) {
    return mainService.loadUser()
        .then(function (response) {
            return response;
        })
        .catch(function (response) {
            return null;
        });
}