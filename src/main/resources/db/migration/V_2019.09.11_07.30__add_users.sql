insert into users (id, phone, password, birth_date, role)
values (6, '9984132513', '$2a$10$qZvAz7RkrtNkWUxylSuBS.Yv0UmvBJgetTCeb6RRXNr5JR7LrlVFq', '08.07.1986', 'ROLE_CLIENT');

insert into users (id, phone, password, birth_date, role)
values (7, '4456456456', '$2a$10$dI.JvotgG/05oEtRCnIYN.26JJB9v5yLVvandJJjONSpKN7.k/1vq', '07.07.1986', 'ROLE_CLIENT');

insert into users (id, phone, password, birth_date, role)
values (8, '5465423287', '$2a$10$pvQ.C4CRROy96IeSudE7Ge.Tntn.04ESt31YEAKlUIts41/Crdtye', '06.07.1986', 'ROLE_CLIENT');