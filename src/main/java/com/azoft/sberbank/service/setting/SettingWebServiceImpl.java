package com.azoft.sberbank.service.setting;

import com.azoft.sberbank.dto.LogDto;
import com.azoft.sberbank.dto.SettingDto;
import com.azoft.sberbank.dto.Topic;
import com.azoft.sberbank.entity.LogType;
import com.azoft.sberbank.entity.Setting;
import com.azoft.sberbank.service.log.LoggingService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SettingWebServiceImpl implements SettingWebService {

    private static final Long SETTING_ID = 1L;
    private final SettingService settingService;
    private final LoggingService loggingService;
    private final CacheManager cacheManager;

    /**
     * Метод получения актуальных настроек
     * @return актуальные настройки
     */
    @Override
    public SettingDto getActualSetting() {
        return new SettingDto(findActualSetting());
    }

    /**
     * Метод обновления настроек
     * @param dto данные для обновления
     * @return обновленные настройки
     */
    @Override
    public SettingDto update(SettingDto dto) {
        Setting setting = findActualSetting();
        loggingService.send(new LogDto(LogType.CHANGE_SETTING, getDiff(setting, dto)), Topic.USER_LOG);
        if (setting.getTimeCache() != dto.getTimeCache()) {
            cacheManager.getCacheNames()
                    .forEach(name -> cacheManager.getCache(name).clear());
        }
        setting.setOnServiceMode(dto.isOnServiceMode());
        setting.setTimeCache(dto.getTimeCache());
        setting.setRequireDate(dto.isRequireDate());
        setting = settingService.saveOrUpdate(setting);
        return new SettingDto(setting);
    }

    /**
     * Метод получения различий между настройками для записи в лог
     * @param setting текущие настройки
     * @param dto измененые данные
     * @return строка с изменениями
     */
    private String getDiff(Setting setting, SettingDto dto) {
        StringBuilder sb = new StringBuilder();
        if (setting.isOnServiceMode() != dto.isOnServiceMode()) {
            sb.append(", Включить сервисный режим: ").append(setting.isOnServiceMode()).append(" => ").append(dto.isOnServiceMode());
        }
        if (setting.isRequireDate() != dto.isRequireDate()) {
            sb.append(", Выводить в форме авторизации поле: ").append(setting.isRequireDate()).append(" => ").append(dto.isRequireDate());
        }
        if (setting.getTimeCache() != dto.getTimeCache()) {
            sb.append(", Время кеширования: ").append(setting.getTimeCache()).append(" => ").append(dto.getTimeCache());
        }
        return sb.length() > 2 ? sb.substring(2): "";
    }

    private Setting findActualSetting() {
        return settingService.findById(SETTING_ID);
    }
}
