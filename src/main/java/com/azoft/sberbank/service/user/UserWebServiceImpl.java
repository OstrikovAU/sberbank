package com.azoft.sberbank.service.user;

import com.azoft.sberbank.dto.UserDto;
import com.azoft.sberbank.entity.Balance;
import com.azoft.sberbank.entity.Role;
import com.azoft.sberbank.entity.User;
import com.azoft.sberbank.security.LoginHelper;
import com.azoft.sberbank.service.balance.BalanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserWebServiceImpl implements UserWebService {

    private final UserService userService;
    private final BalanceService balanceService;

    /**
     * Метод получения авторизованого пользователя
     * @return пользователь
     */
    public UserDto getUser() {
        User user = LoginHelper.getUser();
        user = userService.findById(user.getId());
        Balance balance = null;
        if (Role.ROLE_CLIENT.equals(user.getRole())) {
            balance = balanceService.getBalance(user.getIdData());
        }
        return new UserDto(user, balance);
    }

    /**
     * Метод получения всех пользователей
     * @return список пользователей
     */
    @Override
    public List<UserDto> getUsers() {
        List<User> users = userService.findByRole(Role.ROLE_CLIENT);
        return users.stream().map(UserDto::new).collect(Collectors.toList());
    }
}
