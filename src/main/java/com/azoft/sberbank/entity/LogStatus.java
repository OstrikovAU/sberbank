package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum LogStatus {

    OK("Успех"),
    NO_OK("Неуспех"),
    ERROR("Ошибка");

    /**
     * Описание статуса
     */
    private final String description;
}
