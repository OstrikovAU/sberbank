function NotifyController($rootScope) {

    const self = this;
    self.showNotify = false;
    self.hideNotify = hideNotify;

    $rootScope.$on('notifySuccess:show', notifySuccessShow);
    $rootScope.$on('notifySuccess:hide', notifySuccessHide);
    $rootScope.$on('notifyError:show', notifyErrorShow);
    $rootScope.$on('notifyError:hide', notifyErrorHide);

    function hideNotify() {
        self.showNotify = false;
    }

    function notifySuccessShow(event, text) {
        self.showNotify = true;
        self.isSuccess = true;
        if (text) {
            self.description = text;
        } else {
            self.description = 'Настройки успешно сохранены.';
        }
    }

    function notifySuccessHide() {
        self.showNotify = false;
    }

    function notifyErrorShow(event, text) {
        self.showNotify = true;
        self.isSuccess = false;
        if (text) {
            self.description = text;
        } else {
            self.description = 'Попробуйте изменить настройки через несколько минут.';
        }
    }

    function notifyErrorHide() {
        self.showNotify = false;
    }
}

module.exports = NotifyController;