package com.azoft.sberbank.handler;

import com.azoft.sberbank.dto.ErrorResponse;
import com.azoft.sberbank.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class RestErrorHandler {

    /**
     * Обработка ошибки UserNotFountException
     * @param ex exception
     * @return Объект с сообщением об ошибке
     */
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse loginError(UserNotFoundException ex) {
        log.error(ex.getMessage());
        return new ErrorResponse("Неправильные логин/пароль.");
    }

    /**
     * Обработка всех остальных ошибок на сервере
     * @param ex exception
     * @return Объект с сообщением об ошибке
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse validateError(RuntimeException ex) {
        log.error(ex.getMessage());
        return new ErrorResponse("Внутреняя ошибка сервер");
    }
}
