package com.azoft.sberbank.service.balance;

import com.azoft.sberbank.dto.BalanceCache;

import java.math.BigDecimal;

public interface BalanceCacheService {

    BalanceCache getBalance(String idData);

    void caching(String idData, BigDecimal balance);
}
