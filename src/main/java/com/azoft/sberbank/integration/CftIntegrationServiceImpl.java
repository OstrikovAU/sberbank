package com.azoft.sberbank.integration;

import com.azoft.sberbank.dto.LogDto;
import com.azoft.sberbank.dto.Topic;
import com.azoft.sberbank.dto.UserInfo;
import com.azoft.sberbank.entity.Level;
import com.azoft.sberbank.entity.LogStatus;
import com.azoft.sberbank.entity.LogType;
import com.azoft.sberbank.exception.UserNotFoundException;
import com.azoft.sberbank.integration.schema.*;
import com.azoft.sberbank.service.log.LoggingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CftIntegrationServiceImpl implements CftIntegrationService {

    private static final ParameterizedTypeReference<List<UserInfoResponse>> PARAMETERIZED_RESPONSE_TYPE = new ParameterizedTypeReference<List<UserInfoResponse>>() {};

    @Qualifier("CftRestTemplate")
    private final RestTemplate restTemplate;
    private final LoggingService loggingService;
    private final String apiUrl;
    /**
     * Идентификатор Сети в ПЦ. Выдается Отделом Эксплуатации при регистрации Участника в Сервисе
     */
    private final String network;
    private final String clientInfo;
    private final String level;

    public CftIntegrationServiceImpl(RestTemplate restTemplate, LoggingService loggingService, Environment env) {
        this.restTemplate = restTemplate;
        this.loggingService = loggingService;
        this.apiUrl = env.getProperty("app.cft.url.api");
        this.network = env.getProperty("app.cft.network");
        this.clientInfo = env.getProperty("app.cft.url.client_info");
        this.level = env.getProperty("app.cft.url.level");
    }

    /**
     * Получение информации о пользователе
     * @param phone телефон
     * @param birthDate дата рождения
     * @return Информация о пользователе в пц
     */
    public UserInfo userInfoQuery(String phone, String birthDate) {
        log.info("Запрос в пц для получения данных пользователя {}", phone);
        UserInfo userInfo;
        try {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("phone", phone);
            if (birthDate != null) {
                params.add("birthDate", LocalDate.from(DateTimeFormatter.ofPattern("dd.MM.yyyy").parse(birthDate)).toString());
            }
            URI uri = UriComponentsBuilder.fromUriString(apiUrl).queryParams(params).buildAndExpand(network).toUri();
            RequestEntity<?> requestEntity = new RequestEntity<>(HttpMethod.GET,uri);
            ResponseEntity<List<UserInfoResponse>> response = restTemplate.exchange(requestEntity, PARAMETERIZED_RESPONSE_TYPE);
            loggingService.send(new LogDto(LogType.REQUEST, LogStatus.OK, apiUrl, new ObjectMapper().writeValueAsString(params)), Topic.INTEGRATION_LOG);
            checkStatus(response, apiUrl);
            UserInfoResponse infoResponse = response.getBody().get(0);
            userInfo = infoResponse.getEntities().stream().filter(e -> "CLIENT".equals(e.getType())).findFirst().map(UserInfo::new).orElseThrow(RuntimeException::new);
            log.info("Запрос в пц для получения данных пользователя {} успешно выполнен", phone);
            loggingService.send(new LogDto(LogType.RESPONSE, LogStatus.OK, apiUrl, new ObjectMapper().writeValueAsString(response.getBody())), Topic.INTEGRATION_LOG);
        } catch (Exception ex) {
            log.error("Ошибка запроса к пц для пользователя {}", phone);
            log.error(ex.getMessage());
            throw new UserNotFoundException("Пользователь не найден");
        }
        return userInfo;
    }

    /**
     * Получение баланса пользователя
     * @param idData (id_ow)
     * @return Количество бонусов
     */
    @Override
    public BigDecimal balanceQuery(String idData) {
        log.info("Запрос к пц для получения баланса пользователя idDate={}", idData);
        BigDecimal balance;
        try {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>() {
                {
                    add("showBonusBalance", "true");
                }
            };
            String url = apiUrl + clientInfo;
            URI uri = UriComponentsBuilder.fromUriString(url).queryParams(params).buildAndExpand(network, idData).toUri();
            RequestEntity<?> requestEntity = new RequestEntity<>(HttpMethod.GET, uri);
            ResponseEntity<BalanceInfoResponse> response = restTemplate.exchange(requestEntity, BalanceInfoResponse.class);
            loggingService.send(new LogDto(LogType.REQUEST, LogStatus.OK, url, new ObjectMapper().writeValueAsString(params)), Topic.INTEGRATION_LOG);
            checkStatus(response, url);
            BalanceInfoResponse body = response.getBody();
            balance = Optional.ofNullable(body)
                    .map(BalanceInfoResponse::getBonusBalance)
                    .map(BalanceInfoResponse.BonusBalance::getTotal)
                    .map(b -> b.divide(new BigDecimal(100), 2))
                    .orElse(new BigDecimal(0));
            log.info("Запрос к пц для получения баланса пользователя idDate={} успешно выполнен", idData);
            loggingService.send(new LogDto(LogType.RESPONSE, LogStatus.OK, url, new ObjectMapper().writeValueAsString(body)), Topic.INTEGRATION_LOG);
        } catch (Exception ex) {
            log.error("Ошибка запроса получения баланса для пользователя idData={}", idData);
            log.error(ex.getMessage());
            return new BigDecimal(0);
        }
        return balance;
    }

    /**
     * Получение уровня привелегии
     * @param idData (id_ow)
     * @return Уровень привелегии
     */
    @Override
    public Level levelQuery(String idData) {
        log.info("Запрос к пц для получения уровня привелегий пользователя idDate={}", idData);
        Level level;
        try {
            DetailedInfoRequest request = new DetailedInfoRequest();
            DateRange dateRange = new DateRange();
            dateRange.setStartDate(ZonedDateTime.now().with(TemporalAdjusters.firstDayOfMonth()));
            dateRange.setEndDate(ZonedDateTime.now().with(TemporalAdjusters.lastDayOfMonth()));
            request.getTimeDependentAttributes().add(DetailedInfoAttributes.builder().code("OWI_LEVEL").dateRange(dateRange).build());
            request.getTimeDependentAttributes().add(DetailedInfoAttributes.builder().code("LEVEL2").dateRange(dateRange).build());
            request.getTimeDependentAttributes().add(DetailedInfoAttributes.builder().code("PURCHASED_LEVEL").dateRange(dateRange).build());
            String url = apiUrl + this.level;
            ResponseEntity<DetailedInfoResponse> response = restTemplate.postForEntity(url, request, DetailedInfoResponse.class, network, idData);
            loggingService.send(new LogDto(LogType.REQUEST, LogStatus.OK, url, new ObjectMapper().writeValueAsString(request)), Topic.INTEGRATION_LOG);
            checkStatus(response, url);
            DetailedInfoResponse body = response.getBody();
            level = CftIntegrationHelper.parse(body);
            log.info("Запрос к пц для получения уровня привелегий пользователя idDate={} успешно выполнен", idData);
            loggingService.send(new LogDto(LogType.RESPONSE, LogStatus.OK, url, new ObjectMapper().writeValueAsString(body)), Topic.INTEGRATION_LOG);
        } catch (Exception ex) {
            log.error("Ошибка запроса получения уровня привелегии для пользователя idData={}", idData);
            log.error(ex.getMessage());
            return null;
        }
        return level;
    }

    private void checkStatus(ResponseEntity response, String url) {
        Object body = response.getBody();
        try {
            if (response.getStatusCode().is5xxServerError()) {
                loggingService.send(new LogDto(LogType.RESPONSE, LogStatus.ERROR, url, new ObjectMapper().writeValueAsString(body)), Topic.INTEGRATION_LOG);
            }
            if (response.getStatusCode().is4xxClientError()) {
                loggingService.send(new LogDto(LogType.RESPONSE, LogStatus.NO_OK, url, new ObjectMapper().writeValueAsString(body)), Topic.INTEGRATION_LOG);
            }
        } catch (Exception ex) {
            log.error("Ошибка маппинга ответа");
            log.error(ex.getMessage());
        }

    }

}
