package com.azoft.sberbank.integration.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DetailedInfoRequest {

    /**
     * Список атрибутов на период, для которых требуется вернуть значения
     */
    private List<DetailedInfoAttributes> timeDependentAttributes;

    public List<DetailedInfoAttributes> getTimeDependentAttributes() {
        if (timeDependentAttributes == null) {
            timeDependentAttributes = new ArrayList<>();
        }
        return timeDependentAttributes;
    }
}
