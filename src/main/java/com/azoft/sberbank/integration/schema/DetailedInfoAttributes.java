package com.azoft.sberbank.integration.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DetailedInfoAttributes implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -4520430222545282426L;

    /**
     * Временной диапазон для формирования списка
     */
    private DateRange dateRange;

    /**
     * Код time-dependent атрибута
     */
    private String code;

    /**
     * Тип данных time-dependent атрибута
     * STRING
     * NUMBER
     * BOOLEAN
     */
    private String dataType;

    /**
     * Список значений time-dependent атрибутов за запрашиваемые периоды. Может быть null
     */
    private List<DetailedInfoAttrValue> values;

    public List<DetailedInfoAttrValue> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }
}
