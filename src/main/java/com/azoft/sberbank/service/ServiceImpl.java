package com.azoft.sberbank.service;

import com.azoft.sberbank.entity.AEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Базовый сервис для работы с сущностями
 * @param <E> тип сущности
 * @param <ID> тип ID
 * @param <R> тип репозитория
 */
public abstract class ServiceImpl<E extends AEntity, ID extends Long, R extends JpaRepository<E, ID>> implements Service<E, ID> {

    /**
     * Сохранение или обновление сущности
     * @param entity сущность
     * @return обновленая сущность
     */
    @Override
    @Transactional
    public E saveOrUpdate(E entity) {
        return getRepository().save(entity);
    }

    /**
     * Поиск сущности
     * @param id
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public E findById(ID id) {
        return getRepository().getOne(id);
    }

    protected abstract R getRepository();
}
