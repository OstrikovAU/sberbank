package com.azoft.sberbank.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Объект кэша баланса
 */
@Getter
@Setter
public class BalanceCache {

    /**
     * Дата создания
     */
    private Instant created;
    /**
     * Количество бонусов
     */
    private BigDecimal balance;

    public BalanceCache(Instant created, BigDecimal balance) {
        this.created = created;
        this.balance = balance;
    }

    public boolean isExpired(Instant timeExpired) {
        return created.isBefore(timeExpired);
    }
}
