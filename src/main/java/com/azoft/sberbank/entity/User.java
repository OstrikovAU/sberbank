package com.azoft.sberbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "users")
@Data
public class User extends AEntity {

    /**
     * id записи в бд
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name="user_generator", sequenceName = "user_seq")
    private Long id;

    /**
     * Телефон пользователя
     */
    private String phone;
    /**
     * Дата рождения
     */
    @Column(name = "birth_date")
    private String birthDate;
    /**
     * Пароль
     */
    private String password;
    /**
     * Роль пользователя
     *      ROLE_ADMIN(админ)
     *      ROLE_CLIENT(клиент)
     */
    @Enumerated(EnumType.STRING)
    private Role role;
    /**
     * Уровень привелегии
     *     MORE_THAN_THANKS("Больше, чем Спасибо")
     *     THANKS("Спасибо")
     *     MUCH_THANKS("Большое спасибо")
     *     SO_MUCH_THANKS("Огромное спасибо")
     */
    @Column(name = "user_level")
    @Enumerated(EnumType.STRING)
    private Level level;
    /**
     * Статус
     *     A("Активный")
     *     D("Временно заблокирован")
     *     R("Ограничено списание баллов")
     *     L("Заблокирован (окончательный статус)"
     */
    @Enumerated(EnumType.STRING)
    private Status status;
    /**
     * id_ow
     */
    @Column(name = "id_data")
    private String idData;
    /**
     * Баланс
     */
    @Embedded
    private Balance balance;
}
