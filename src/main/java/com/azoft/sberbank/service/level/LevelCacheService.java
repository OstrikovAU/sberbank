package com.azoft.sberbank.service.level;

import com.azoft.sberbank.dto.LevelCache;
import com.azoft.sberbank.entity.Level;

public interface LevelCacheService {

    LevelCache getLevel(String idData);

    void caching(String idData, Level level);
}
