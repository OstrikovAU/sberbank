package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Status {

    A("Активный"),
    D("Временно заблокирован"),
    R("Ограничено списание баллов"),
    L("Заблокирован (окончательный статус)");

    /**
     * Описание статуса пользователя
     */
    private final String description;
}
