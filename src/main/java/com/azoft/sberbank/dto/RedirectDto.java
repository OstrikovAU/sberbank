package com.azoft.sberbank.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RedirectDto {

    /**
     * Название страницы для редиректа
     */
    private String page;

    public RedirectDto(String page) {
        this.page = page;
    }
}
