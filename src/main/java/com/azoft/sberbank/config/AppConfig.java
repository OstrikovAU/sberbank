package com.azoft.sberbank.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Optional;

@Configuration
@Slf4j
public class AppConfig {

    @Bean(name = "CftRestTemplate")
    public static RestTemplate restTemplate(Environment env) {
        String keystorePath = Optional.ofNullable(env.getProperty("app.cft.keystore.path")).orElse("");
        String truststorePath = Optional.ofNullable(env.getProperty("app.cft.truststore.path")).orElse("");
        String keystorePassword = Optional.ofNullable(env.getProperty("app.cft.keystore.password")).orElse("");
        String truststorePassword = Optional.ofNullable(env.getProperty("app.cft.truststore.password")).orElse("");
        return new RestTemplate(getHttpClientFactory(keystorePath, keystorePassword, truststorePath, truststorePassword));
    }

    @Bean
    public static CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("balance", "level");
    }


    public static ClientHttpRequestFactory getHttpClientFactory(String keystorePath, String keystorePassword,
                                                                          String truststorePath, String truststorePassword) {
        try {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream keystoreInput = new FileInputStream(keystorePath);
            keystore.load(keystoreInput, keystorePassword.toCharArray());
            keystoreInput.close();
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream trustStoreInput = new FileInputStream(truststorePath);
            trustStore.load(trustStoreInput, truststorePassword.toCharArray());
            trustStoreInput.close();

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                    KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keystore, keystorePassword.toCharArray());
            KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
            SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(keyManagers, trustManagers, null);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                    .<ConnectionSocketFactory>create().register("https", new SSLConnectionSocketFactory(sslcontext))
                    .build();
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            cm.setMaxTotal(100);
            cm.setDefaultMaxPerRoute(100);
            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(60000)
                    .setConnectionRequestTimeout(60000)
                    .build();
            return new HttpComponentsClientHttpRequestFactory(HttpClients
                    .custom()
                    .setDefaultRequestConfig(config)
                    .setConnectionManager(cm)
                    .setSSLContext(sslcontext)
                    .build());

        } catch (Exception e) {
            log.error("Can't init cftHttpClient", e);
            throw new RuntimeException(e);
        }
    }

}
