package com.azoft.sberbank.service.balance;

import com.azoft.sberbank.dto.BalanceCache;
import com.azoft.sberbank.entity.Balance;
import com.azoft.sberbank.integration.CftIntegrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.Instant;

@Service
@RequiredArgsConstructor
public class BalanceServiceImpl implements BalanceService {

    private final CftIntegrationService cftIntegrationService;
    private final BalanceCacheService balanceCacheService;

    /**
     * Метод получения баланса пользователя
     * @param idData (id_ow)
     * @return Баланс
     */
    @Override
    public Balance getBalance(String idData) {
        BalanceCache balanceCache = balanceCacheService.getBalance(idData);
        Balance balance;
        if (ObjectUtils.isEmpty(balanceCache)) {
            BigDecimal amount = cftIntegrationService.balanceQuery(idData);
            balanceCacheService.caching(idData, amount);
            balance = new Balance(amount, Instant.now());
        } else {
            balance = new Balance(balanceCache.getBalance(), balanceCache.getCreated());
        }
        return balance;
    }
}
