package com.azoft.sberbank.service.level;

import com.azoft.sberbank.entity.Level;

public interface LevelService {

    Level getLevel(String idData);
}
