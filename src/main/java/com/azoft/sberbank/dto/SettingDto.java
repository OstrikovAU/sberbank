package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.Setting;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class SettingDto {

    /**
     * Время кэширования запросов
     */
    private int timeCache;
    /**
     * Включение сервисного режима
     */
    private boolean onServiceMode;
    /**
     * Обязательность даты рождения
     */
    private boolean requireDate;

    public SettingDto(Setting setting) {
        this.timeCache = setting.getTimeCache();
        this.onServiceMode = setting.isOnServiceMode();
        this.requireDate = setting.isRequireDate();
    }
}
