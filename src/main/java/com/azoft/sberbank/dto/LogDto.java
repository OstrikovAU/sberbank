package com.azoft.sberbank.dto;

import com.azoft.sberbank.entity.LogStatus;
import com.azoft.sberbank.entity.LogType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogDto {

    /**
     * Тип дайствия
     *     REQUEST("Запрос")
     *     RESPONSE("Ответ")
     *     LOGIN("Авторизация")
     *     LOGOUT("Деавторизация")
     *     EXPORT("Выгрузка файла")
     *     CHANGE_SETTING("Изменение настроек")
     */
    private LogType type;
    /**
     * Параметры
     */
    private String params;
    /**
     * Название метода
     */
    private String methodName;
    /**
     * Статус
     *     OK("Успех")
     *     NO_OK("Неуспех")
     *     ERROR("Ошибка")
     */
    private LogStatus status;

    public LogDto() {
    }

    public LogDto(LogType type, String params) {
        this.type = type;
        this.params = params;
    }

    public LogDto(LogType type, LogStatus status, String methodName, String params) {
        this.type = type;
        this.params = params;
        this.methodName = methodName;
        this.status = status;
    }
}
