package com.azoft.sberbank.service.setting;

import com.azoft.sberbank.entity.Setting;
import com.azoft.sberbank.repository.SettingRepository;
import com.azoft.sberbank.service.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SettingServiceImpl extends ServiceImpl<Setting, Long, SettingRepository> implements SettingService {

    private final SettingRepository repository;

    @Override
    protected SettingRepository getRepository() {
        return repository;
    }
}
