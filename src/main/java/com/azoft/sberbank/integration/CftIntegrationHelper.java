package com.azoft.sberbank.integration;

import com.azoft.sberbank.entity.Level;
import com.azoft.sberbank.integration.schema.DetailedInfoAttrValue;
import com.azoft.sberbank.integration.schema.DetailedInfoAttributes;
import com.azoft.sberbank.integration.schema.DetailedInfoResponse;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

public final class CftIntegrationHelper {

    private static final String LEVEL_ERROR_MSG = "Уровень пользователя не определен";

    private CftIntegrationHelper() {
    }

    public static Level parse(DetailedInfoResponse response) {
        List<DetailedInfoAttributes> infoAttributes = Optional.ofNullable(response)
                .map(DetailedInfoResponse::getTimeDependentAttributes)
                .orElseThrow(() -> new RuntimeException(LEVEL_ERROR_MSG));
        DetailedInfoAttributes level2 = null;
        DetailedInfoAttributes owiLevel = null;
        DetailedInfoAttributes purchasedLevel = null;
        for (DetailedInfoAttributes info: infoAttributes) {
            if ("LEVEL2".equals(info.getCode()) && !CollectionUtils.isEmpty(info.getValues())) {
                level2 = info;
            } else if ("OWI_LEVEL".equals(info.getCode()) && !CollectionUtils.isEmpty(info.getValues())) {
                owiLevel = info;
            } else if ("PURCHASED_LEVEL".equals(info.getCode()) && !CollectionUtils.isEmpty(info.getValues())) {
                purchasedLevel = info;
            }
        }
        return Optional.ofNullable(owiLevel != null? owiLevel: purchasedLevel != null? purchasedLevel: level2)
                .map(v -> v.getValues().get(0))
                .map(DetailedInfoAttrValue::getValue)
                .map(Level::parse)
                .orElseThrow(() -> new RuntimeException(LEVEL_ERROR_MSG));
    }
}
