package com.azoft.sberbank.export;

import com.azoft.sberbank.entity.AEntity;
import com.azoft.sberbank.service.log.LogService;

import java.io.IOException;
import java.time.*;
import java.util.List;

public abstract class AbstractCsvExportProcessor<T extends AEntity, S extends LogService<T, Long>> {
    private final S service;
    private CsvExport csvExport;


    AbstractCsvExportProcessor(S service, String encoding) {
        this.service = service;
        this.csvExport = new CsvExport(encoding);
    }

    /**
     * Запуск процесса экспорта
     * @return байты данных
     */
    public byte[] process() {
        byte[] result;
        try {
            createTableHead();


            Instant start = ZonedDateTime.of(LocalDate.now().atStartOfDay(), ZoneOffset.systemDefault()).toInstant();
            Instant end = Instant.now();
            List<T> entities = service.find(start, end);
            for (T entity: entities) {
                exportEntity(entity);
            }
            result = getByte();
        } catch (IOException ex) {
            throw new RuntimeException();
        }
        return result;
    }

    /**
     * Метод получения объекта для формирования файла csv
     * @return Объект для построения файла csv
     */
    CsvExport getCsvExport() {
        return csvExport;
    }

    /**
     * Метод получения сформированных данных
     * @return сформированные данные
     * @throws IOException
     */
    private byte[] getByte() throws IOException {
        return getCsvExport().write().toByteArray();
    }

    /**
     * Метод формирования строк данных
     * @param bean сущность
     */
    abstract void exportEntity(T bean);

    /**
     * Метод формирования заголовка файла
     */
    abstract void createTableHead();

}
