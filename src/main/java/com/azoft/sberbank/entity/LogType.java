package com.azoft.sberbank.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum LogType {

    REQUEST("Запрос"),
    RESPONSE("Ответ"),
    LOGIN("Авторизация"),
    LOGOUT("Деавторизация"),
    EXPORT("Выгрузка файла"),
    CHANGE_SETTING("Изменение настроек");

    /**
     * Описание типа лога
     */
    private final String description;
}
