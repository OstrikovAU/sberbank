function HeaderController($rootScope, mainService, $location) {

    const self = this;
    self.sigin = sigin;
    self.logout = logout;

    $rootScope.$on('update', update);
    $rootScope.$on('$routeChangeStart', onRouteChangeStart);
    $rootScope.$on('$routeChangeSuccess', onRouteChangeSuccess);

    function onRouteChangeSuccess() {
        const path = $location.path();
        const user = self.user;
        if ((path === '/' || path === '/login' || path === '/error' || path === '/tech') && !user) {
            $rootScope.hideView = false;
            return;
        }
        if ((path !== '/' || path !== '/login' || path !== '/error' || path !== '/tech') && !user) {
            $location.path('');
        } else if (path === '/admin' && user.role === 'ROLE_CLIENT') {
            $location.path('');
        } else if (path === '/' && user.role === 'ROLE_ADMIN') {
            $location.path('/admin');
        }
        $rootScope.hideView = false;
    }

    function onRouteChangeStart() {
        $rootScope.hideView = true;
        $rootScope.isAdmin = $location.path() === '/admin';
        if ($location.path() === '/login' && self.user) {
            $location.path('');
        }
    }

    function update(event, setting, user) {
        self.user = user;
        self.setting = setting;
    }

    function logout() {
        mainService.logout();
        $rootScope.$emit('update');
        $location.path('login');
    }

    function sigin() {
        $location.path('login');
    }
}

module.exports = HeaderController;