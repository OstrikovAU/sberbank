function LoaderDirective() {
    return {
        restrict: 'E',
        scope: {},
        controller: 'loaderController',
        controllerAs: 'loaderCtrl',
        templateUrl: 'common/loader/loader.html'
    }
}

module.exports = LoaderDirective;