function PhoneFormatFilter() {
    return function (value) {
        const codeCountry = '+7';
        const codeCity = value.substring(0, 3);
        const block1 = value.substring(3, 6);
        const block2 = value.substring(6, 8);
        const block3 = value.substring(8);
        return codeCountry + ' (' + codeCity + ') ' + block1 + '-' + block2 + '-' + block3;
    }
}

module.exports = PhoneFormatFilter;