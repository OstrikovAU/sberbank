package com.azoft.sberbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "app_setting")
@Data
public class Setting extends AEntity {

    /**
     * id записи в бд
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "setting_generator")
    @SequenceGenerator(name="setting_generator", sequenceName = "setting_seq")
    private Long id;
    /**
     * Время кэширования запросов min 0 max 60
     */
    @Column(name = "time_cache")
    @Min(0)
    @Max(60)
    private int timeCache;
    /**
     * Включение сервисного режима вкл=true выкл=false
     */
    @Column(name = "on_service_mode")
    private boolean onServiceMode;
    /**
     * Обязательность даты рождения да=true нет=false
     */
    @Column(name = "require_date")
    private boolean requireDate;
}
