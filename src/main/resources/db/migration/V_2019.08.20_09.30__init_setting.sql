create table setting (
    id bigint primary key,
    on_service_mode boolean not null,
    require_date boolean not null,
    time_cache int not null
);

insert into setting (id, on_service_mode, require_date, time_cache) VALUES (1, false, true, 0);