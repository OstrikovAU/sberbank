function MainController($rootScope, user) {

    const self = this;
    self.user = user;

    $rootScope.$emit('update', null, self.user);
}

module.exports = MainController;