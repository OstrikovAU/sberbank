package com.azoft.sberbank.service.auth;

import com.azoft.sberbank.dto.LoginRequest;

public interface AuthWebService {

    String login(LoginRequest loginRequest);

    void logout();
}
